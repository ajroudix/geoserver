<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends AbstractController
{

    /**
     * @return JsonResponse
     * @Route("/{adr}/{ip}",name="home", methods={"get"})
     */
    public function index($adr,$ip):JsonResponse
    {
        //$adr = "24 Avenue Henri BERGSON, 92380 Garches, France";
        //$ip = "8.8.8.8";

        //step 1
        $location1 = $this->getLocationByIp($ip);

        //step 2
        $location2 = $this->getLocationByAddress($adr);

        //step 3
        $dis = $this->distance($location1['lat'], $location1['lng'], $location2['lat'], $location2['lng'], "K");

        return new JsonResponse($dis);
    }




// functions
    public function getLocationByIp($ip)
    {

        $api_key = 'at_e30TSHlsqY8YmGwSQbnDl6Sf50XhT';
        $api_url = 'https://geo.ipify.org/api/v1';
        $ip = str_replace('+', '.', $ip);
        $url = "{$api_url}?apiKey={$api_key}&ipAddress={$ip}";

        $json = file_get_contents($url);

        $tab = json_decode($json, true);

        $lat = $tab['location']['lat'];
        $lng = $tab['location']['lng'];

        return [
            'lat' => $lat,
            'lng' => $lng
        ];
    }

    public function getLocationByAddress($adr)
    {
        //$adrUrlFormat = str_replace(' ', '+', $adr);
        $key = 'AIzaSyB0HeQ6Fmkw-pWYnJO44S-cq4HCl3IW1-o';
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$adr}&key={$key}";
        $file = file_get_contents($url);
        $tab = json_decode($file, true);
        $lati = $tab['results'][0]['geometry']['location']['lat'];
        $longi = $tab['results'][0]['geometry']['location']['lng'];
        return [
            'lat' => $lati,
            'lng' => $longi
        ];
    }


    public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
}