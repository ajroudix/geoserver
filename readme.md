## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Test](#test)

## General info
Server-Geocoding is to calculate the distance between a postal address and an IP address.
the form is filled and submitted, the request must be sent to a second micro service to do the
distance calculation.
Finally, the distance calculation must be displayed in kilometers.
	
## Technologies
Server-Geocoding is created with:
* Symfony : 5.0.2
* PHP : 7.4
* Docker : 18.09.7, build 2d0083d
	
## Setup
To run this project, follow the following step

```
$ git clone https://ajroudix@bitbucket.org/ajroudix/geoserver.git
$ cd geoserver
$ composer update
$ make compose
```
## Test
you can use this link as an example to see the result:

* http://localhost:81/24+avenue+Henri+Bergson,+92380+Garches+France/8+8+8+8